package com.ads.web.error.handling

import com.ads.web.error.ErrorMessage
import com.ads.web.error.HttpErrorMessage

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.autoconfigure.web.ErrorProperties
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.http.HttpStatus
import spock.lang.Specification
import spock.lang.Unroll

import javax.servlet.http.HttpServletRequest
import java.sql.SQLException

class CustomBasicErrorControllerSpec extends Specification {

    def resolver = ErrorResolverImpl
            .builder(HttpStatus.INTERNAL_SERVER_ERROR, 500, "default error", "internal error occurred")
            .byStatus(HttpStatus.NOT_FOUND, 404, "not found")
            .byException(IOException, HttpStatus.INSUFFICIENT_STORAGE, 507, "io error", "io error occurred on server")
            .byException(IllegalArgumentException, HttpStatus.BAD_REQUEST, 400, "bad request")
            .byException(SQLException.class, { ex ->
                def httpErrorMessage = null
                if (ex.getSQLState() == "42") {
                    def errorMessage = new ErrorMessage(42, "Sql error message", "Sql auxiliary error message")
                    httpErrorMessage = new HttpErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage)
                } else if (ex.getSQLState() == "41") {
                    def errorMessage = new ErrorMessage(41, "Constraint Violation", "Constraint Violation")
                    httpErrorMessage = new HttpErrorMessage(HttpStatus.UNPROCESSABLE_ENTITY, errorMessage)
                }
                return httpErrorMessage
            })
            .build()

    def controller = new CustomBasicErrorController(new DefaultErrorAttributes(), new ErrorProperties(), resolver, new ObjectMapper())


    def "Should resolve error message by exception, if it's present in request attribute"() {
        given: "http request"
        def request = Mock HttpServletRequest

        and: "exception in that request attribute"
        request.getAttribute('javax.servlet.error.exception') >> new IOException()

        when: "rendering error response"
        def result = controller.error(request)

        then: "result is correct"
        result.statusCode == HttpStatus.INSUFFICIENT_STORAGE
        result.body['error-code'] == 507
        result.body['error-message'] == 'io error'
        result.body['error-auxiliary-message'] == 'io error occurred on server'
    }

    @Unroll
    def "Should resolve error message by exception with specific data #message, if it's present in request attribute"() {
        given: "http request"
        def request = Mock HttpServletRequest

        and: "exception in that request attribute"
        request.getAttribute('javax.servlet.error.exception') >> new SQLException("Sql exception", code.toString())

        when: "rendering error response"
        def result = controller.error(request)

        then: "result is correct"
        result.statusCode == status
        result.body['error-code'] == code
        result.body['error-message'] == message
        result.body['error-auxiliary-message'] == auxMessage

        where:
        code | message                | auxMessage                    | status
        42   | "Sql error message"    | "Sql auxiliary error message" | HttpStatus.INTERNAL_SERVER_ERROR
        41   | "Constraint Violation" | "Constraint Violation"        | HttpStatus.UNPROCESSABLE_ENTITY
        500  | "default error"        | "internal error occurred"     | HttpStatus.INTERNAL_SERVER_ERROR
    }

    def "Should resolve error message by status, if exception isn't present in request attribute"() {
        given: "http request"
        def request = Mock HttpServletRequest

        and: "no exception in that request attribute"
        request.getAttribute('javax.servlet.error.exception') >> null

        and: "status code in request attribute"
        request.getAttribute('javax.servlet.error.status_code') >> 404

        when: "rendering error response"
        def result = controller.error(request)

        then: "result is correct"
        result.statusCode == HttpStatus.NOT_FOUND
        result.body['error-code'] == 404
        result.body['error-message'] == 'not found'
        result.body['error-auxiliary-message'] == HttpStatus.NOT_FOUND.getReasonPhrase()
    }

}
