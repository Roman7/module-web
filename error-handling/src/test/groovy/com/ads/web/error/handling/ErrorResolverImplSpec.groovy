package com.ads.web.error.handling

import com.ads.web.error.ErrorMessage
import com.ads.web.error.HttpErrorMessage

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import spock.lang.Specification
import spock.lang.Unroll

import java.sql.SQLException

class ErrorResolverImplSpec extends Specification {

    def resolver = ErrorResolverImpl
            .builder(HttpStatus.INTERNAL_SERVER_ERROR, 500, "default error", "internal error occurred")
            .byStatus(HttpStatus.NOT_FOUND, 404, "not found")
            .byException(Exception.class, HttpStatus.SERVICE_UNAVAILABLE, 503, "service unavailable", "")
            .byException(CustomException.class, HttpStatus.UNPROCESSABLE_ENTITY, 422, "custom exception", { e -> e.customField })
            .byException(IOException.class, HttpStatus.INSUFFICIENT_STORAGE, 507, "io error", "io error occurred on server")
            .byException(IllegalArgumentException.class, HttpStatus.BAD_REQUEST, 400, "bad request")
            .byException(SQLException.class, { ex ->
                def httpErrorMessage = null
                if (ex.getSQLState() == "42") {
                    def errorMessage = new ErrorMessage(42, "Sql error message", "Sql auxiliary error message")
                    httpErrorMessage = new HttpErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage)
                } else if (ex.getSQLState() == "41") {
                    def errorMessage = new ErrorMessage(41, "Constraint Violation", "Constraint Violation")
                    httpErrorMessage = new HttpErrorMessage(HttpStatus.UNPROCESSABLE_ENTITY, errorMessage)
                }
                return httpErrorMessage
            })
            .build()

    def "Should resolve to parent exception mapping if no mapping for specified exception is found"() {
        when:
        ResponseEntity<ErrorMessage> resolvedMessage = resolver.resolve(new RuntimeException())

        then:
        resolvedMessage.statusCode == HttpStatus.SERVICE_UNAVAILABLE
        resolvedMessage.body.code == 503
        resolvedMessage.body.message == "service unavailable"
        resolvedMessage.body.auxMessage == ""
    }

    def "Should resolve to default mapping when no mapping for exception type nor its parent type is found"() {
        when:
        ResponseEntity<ErrorMessage> resolvedMessage = resolver.resolve(new Throwable())

        then:
        resolvedMessage.statusCode == HttpStatus.INTERNAL_SERVER_ERROR
        resolvedMessage.body.code == 500
        resolvedMessage.body.message == "default error"
        resolvedMessage.body.auxMessage == "internal error occurred"
    }

    def "Should return true when mapping is configured for exception type"() {
        when:
        boolean result = resolver.isConfiguredForHandling(exceptionType)

        then:
        result == expectedResult

        where:
        exceptionType            | expectedResult
        IllegalArgumentException | true
        IOException              | true
        RuntimeException         | true
    }

    def "Should return false when no mapping is configured for exception type nor for it parent type"() {
        when:
        boolean result = resolver.isConfiguredForHandling(Throwable)

        then:
        !result
    }

    def "Should resolve to error message when mapping for provided status is configured"() {
        when:
        def ex = new Exception("helpful information")
        ResponseEntity<ErrorMessage> response = resolver.resolve(HttpStatus.NOT_FOUND, ex)

        then:
        response.statusCode == HttpStatus.NOT_FOUND

        def errorMessage = response.body
        errorMessage.code == 404
        errorMessage.message == "not found"
        errorMessage.auxMessage == ex.message
    }

    def "Should resolve to default error message when no mapping for provided status is configured"() {
        when:
        ResponseEntity<ErrorMessage> response = resolver.resolve(HttpStatus.I_AM_A_TEAPOT, new Exception())

        then:
        response.statusCode == HttpStatus.I_AM_A_TEAPOT

        def errorMessage = response.body
        errorMessage.code == 500
        errorMessage.message == "default error"
        errorMessage.auxMessage == "internal error occurred"
    }

    def "Should include correct values in resolved error message"() {
        when:
        ResponseEntity<ErrorMessage> resolvedMessage = resolver.resolve(exception)

        then:
        resolvedMessage.body.message == errorMessage
        resolvedMessage.body.auxMessage == auxMessage
        resolvedMessage.body.code == errorCode
        resolvedMessage.statusCode == status

        where:
        exception                                                  | errorMessage          | auxMessage                            | errorCode | status
        new Exception()                                            | "service unavailable" | ""                                    | 503       | HttpStatus.SERVICE_UNAVAILABLE
        new CustomException("message from custom exception field") | "custom exception"    | "message from custom exception field" | 422       | HttpStatus.UNPROCESSABLE_ENTITY
        new IOException()                                          | "io error"            | "io error occurred on server"         | 507       | HttpStatus.INSUFFICIENT_STORAGE
        new IllegalArgumentException("some error message")         | "bad request"         | "some error message"                  | 400       | HttpStatus.BAD_REQUEST
    }

    @Unroll
    def "Should return sql expected error message #message"() {
        given: "sql exception with specific data"
        def exception = new SQLException("Sql exception", code.toString())

        when: "resolving exception"
        def resolvedMessage = resolver.resolve(exception)

        then: "exception resolved"
        resolvedMessage.statusCode == status
        resolvedMessage.body.code == code
        resolvedMessage.body.message == message
        resolvedMessage.body.auxMessage == auxMessage

        where:
        code | message                | auxMessage                    | status
        42   | "Sql error message"    | "Sql auxiliary error message" | HttpStatus.INTERNAL_SERVER_ERROR
        41   | "Constraint Violation" | "Constraint Violation"        | HttpStatus.UNPROCESSABLE_ENTITY
        500  | "default error"        | "internal error occurred"     | HttpStatus.INTERNAL_SERVER_ERROR
    }

    def static class CustomException extends RuntimeException {
        String customField

        CustomException(customField) {
            this.customField = customField
        }
    }

}
