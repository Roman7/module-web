package com.ads.web.error.handling

import com.ads.web.error.ErrorMessage
import com.ads.web.error.HttpErrorMessage

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import spock.lang.Specification
import spock.lang.Unroll

import java.sql.SQLException

class CommonErrorHandlerSpec extends Specification {

    def static class CustomException extends RuntimeException {}

    def resolver = ErrorResolverImpl
            .builder(HttpStatus.INTERNAL_SERVER_ERROR, 500, "default error", "internal error occurred")
            .byStatus(HttpStatus.NOT_FOUND, 404, "not found", "resource not found")
            .byException(IOException, HttpStatus.INSUFFICIENT_STORAGE, 507, "io error", "io error occurred on server")
            .byException(IllegalArgumentException, HttpStatus.BAD_REQUEST, 400, "bad request")
            .byException(SQLException.class, { ex ->
                def httpErrorMessage = null
                if (ex.getSQLState() == "42") {
                    def errorMessage = new ErrorMessage(42, "Sql error message", "Sql auxiliary error message")
                    httpErrorMessage = new HttpErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage)
                } else if (ex.getSQLState() == "41") {
                    def errorMessage = new ErrorMessage(41, "Constraint Violation", "Constraint Violation")
                    httpErrorMessage = new HttpErrorMessage(HttpStatus.UNPROCESSABLE_ENTITY, errorMessage)
                }
                return httpErrorMessage
            })
            .build()

    def handler = new CommonErrorHandler(resolver)

    def "Should resolve exception against error resolver when handling exception"() {
        given: "exception with configured mapping"
        def e = new IllegalArgumentException()

        when: "handling given exception"
        def result = handler.handleException(e)

        then: "result is properly resolved"
        result.statusCode == HttpStatus.BAD_REQUEST
        result.body.code == 400
        result.body.message == "bad request"
    }

    def "Should resolve by exception first when handling exception internally"() {
        given: "exception with configured mapping"
        def e = new IOException()

        when: "handling given exception internally"
        def result = handler.handleExceptionInternal(e, null, HttpHeaders.EMPTY, HttpStatus.OK, null) as ResponseEntity<ErrorMessage>

        then: "result is properly resolved"
        result.statusCode == HttpStatus.INSUFFICIENT_STORAGE
        result.body.code == 507
        result.body.message == "io error"
        result.body.auxMessage == "io error occurred on server"
    }

    @Unroll
    def "Should resolve by custom exception with message #message handling"() {
        given: "sql exception with specific data"
        def e = new SQLException("Sql exception", code.toString())

        when: "handling given exception"
        def resolvedMessage = handler.handleException(e)

        then: "result is properly resolved"
        resolvedMessage.statusCode == status
        resolvedMessage.body.code == code
        resolvedMessage.body.message == message
        resolvedMessage.body.auxMessage == auxMessage

        where:
        code | message                | auxMessage                    | status
        42   | "Sql error message"    | "Sql auxiliary error message" | HttpStatus.INTERNAL_SERVER_ERROR
        41   | "Constraint Violation" | "Constraint Violation"        | HttpStatus.UNPROCESSABLE_ENTITY
        500  | "default error"        | "internal error occurred"     | HttpStatus.INTERNAL_SERVER_ERROR
    }

    def "Should resolve by status when exception mapping is not configured when handling exception internally"() {
        given: "exception without mapping"
        def e = new CustomException()
        assert !resolver.isConfiguredForHandling(e.class)

        and: "mapping for specified status configured"
        def status = HttpStatus.NOT_FOUND

        when: "handling given exception internally"
        def result = handler.handleExceptionInternal(e, null, HttpHeaders.EMPTY, status, null) as ResponseEntity<ErrorMessage>

        then: "result resolved by status"
        result.statusCode == status
        result.body.code == 404
        result.body.message == "not found"
    }

}
