package com.ads.web.error.handling;

import com.ads.web.error.ErrorMessage;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
@AllArgsConstructor
public class CommonErrorHandler extends ResponseEntityExceptionHandler {

    private final ErrorResolver errorResolver;

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(Exception ex) {
        ResponseEntity<ErrorMessage> responseEntity = errorResolver.resolve(ex);
        if (responseEntity.getStatusCode().is4xxClientError()) {
            //            log.warn(LoggingMarkers.SENSITIVE, ex.getMessage(), ex);
            System.out.println("is4xxClientError" + ex.toString());
        } else if (responseEntity.getStatusCode().is5xxServerError()) {
            //            log.error(LoggingMarkers.SENSITIVE, "Exception occurred", ex);
            System.out.println("is5xxServerError = " + ex.toString());

        }
        return responseEntity;
    }

    /**
     * Make standard spring's exception handler {@link ResponseEntityExceptionHandler} return error messages in project
     * compliant format. Delegates exception handling to {@link CommonErrorHandler#handleException(java.lang.Exception)
     * } if the last one is mapped in {@link CommonErrorHandler#errorResolver}
     */
    @SuppressWarnings("unchecked")
    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        if (errorResolver.isConfiguredForHandling(ex.getClass())) {
            return (ResponseEntity) handleException(ex);
        }
        //        log.error(LoggingMarkers.SENSITIVE, "Exception occurred during request processing", ex);
        System.out.println("Exception occurred during request processing" + ex.toString());
        return (ResponseEntity) errorResolver.resolve(status, ex);
    }

}
