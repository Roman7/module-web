package com.ads.web.error.handling;

import com.ads.web.error.ErrorMessage;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public interface ErrorResolver {

    /**
     * Resolves response with error message by configured mappings
     *
     * @param status {@link HttpStatus} to resolve error message by
     * @param throwable to resolve error message by
     * @return {@link ResponseEntity} with resolved error message and status
     */
    ResponseEntity<ErrorMessage> resolve(HttpStatus status, Throwable throwable);

    /**
     * Resolves error message by provided http status. If no mapping for provided status is configured then default
     * error is returned.
     *
     * @param status {@link HttpStatus} to resolve error message by
     * @return {@link ResponseEntity} with resolved error message and given status
     */
    ResponseEntity<ErrorMessage> resolve(HttpStatus status);


    /**
     * Resolves response with error message by provided throwable instance. If no mapping for provided exception is
     * returned then default error is returned.
     *
     * @param throwable to resolve error message by
     * @return {@link ResponseEntity} with resolved error message and status
     */
    ResponseEntity<ErrorMessage> resolve(Throwable throwable);

    /**
     * Checks whether error resolver is configured to handle provided exception type
     *
     * @param exceptionType type of exception
     * @return true if error resolver has configuration to handle given exception type
     */
    boolean isConfiguredForHandling(Class<? extends Throwable> exceptionType);

}
