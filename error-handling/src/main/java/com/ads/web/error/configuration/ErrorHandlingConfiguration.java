package com.ads.web.error.configuration;

import com.ads.web.error.handling.CustomBasicErrorController;
import com.ads.web.error.handling.CommonErrorHandler;
import com.ads.web.error.handling.ErrorResolver;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ErrorHandlingConfiguration {

    private final ErrorResolver errorResolver;
    private final ErrorAttributes errorAttributes;
    private final ServerProperties serverProperties;
    private final ObjectMapper mapper;

    public ErrorHandlingConfiguration(
            ErrorResolver errorResolver,
            ErrorAttributes errorAttributes,
            ServerProperties serverProperties,
            ObjectMapper mapper) {
        this.errorResolver = errorResolver;
        this.errorAttributes = errorAttributes;
        this.serverProperties = serverProperties;
        this.mapper = mapper;
    }

    @Bean
    CommonErrorHandler errorHandler() {
        return new CommonErrorHandler(errorResolver);
    }

    @Bean
    CustomBasicErrorController errorController() {
        return new CustomBasicErrorController(errorAttributes, serverProperties.getError(), errorResolver, mapper);
    }
}
