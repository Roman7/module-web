package com.ads.web.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public class HttpErrorMessage {

    private HttpStatus httpStatus;
    private ErrorMessage errorMessage;

}
