package com.ads.web.error.handling;

import static java.util.Objects.nonNull;

import com.ads.web.error.ErrorMessage;
import com.ads.web.error.HttpErrorMessage;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.function.Function;

@Getter
class ErrorMapping {

    private HttpErrorMessage httpErrorMessage;
    private Function<Throwable, String> auxMessageExtractor;
    private Function<Throwable, HttpErrorMessage> customHttpErrorMessageExtractor;

    ErrorMapping(HttpStatus httpStatus, int errorCode, String errorMessage, String auxMessage) {
        this.httpErrorMessage = new HttpErrorMessage(httpStatus, new ErrorMessage(errorCode, errorMessage, auxMessage));
    }

    ErrorMapping(HttpStatus httpStatus, int errorCode, String errorMessage) {
        this.httpErrorMessage = new HttpErrorMessage(httpStatus, new ErrorMessage(errorCode, errorMessage, null));
    }

    ErrorMapping(Function<Throwable, String> auxMessageExtractor, HttpStatus httpStatus,
            int errorCode, String errorMessage) {
        this.auxMessageExtractor = auxMessageExtractor;
        this.httpErrorMessage = new HttpErrorMessage(httpStatus, new ErrorMessage(errorCode, errorMessage, null));
    }

    ErrorMapping(Function<Throwable, HttpErrorMessage> customHttpErrorMessageExtractor) {
        this.customHttpErrorMessageExtractor = customHttpErrorMessageExtractor;
    }

    boolean isCommonErrorMapping() {
        return nonNull(httpErrorMessage)
                && nonNull(httpErrorMessage.getHttpStatus())
                && nonNull(httpErrorMessage.getErrorMessage())
                && httpErrorMessage.getErrorMessage().getCode() != 0
                && nonNull(httpErrorMessage.getErrorMessage().getMessage());
    }

}
