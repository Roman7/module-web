package com.ads.web.error;


import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@JsonAutoDetect(getterVisibility = NONE, setterVisibility = NONE, fieldVisibility = ANY)
public class ErrorMessage {

    /**
     * Micro service error code (XXXYYZZZ): XXX - Micro service code YY - Internal error code ZZZ - Http status error
     * code
     */
    @JsonProperty("error-code")
    private int code;
    @JsonProperty("error-message")
    private String message;
    @JsonProperty("error-auxiliary-message")
    private String auxMessage;

}
