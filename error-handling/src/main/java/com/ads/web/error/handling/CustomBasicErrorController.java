package com.ads.web.error.handling;

import com.ads.web.error.ErrorMessage;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.ServletWebRequest;

import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;

/**
 * Extension of the BasicErrorController, which wraps {@link BasicErrorController#error(HttpServletRequest)} in order to
 * produce error message in project compliant format.
 *
 * See <a href="https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-developing-web-applications.html#boot-features-error-handling">Spring
 * Error handling</a>
 */
@Controller
public class CustomBasicErrorController extends BasicErrorController {

    private static final TypeReference<Map<String, Object>> MAP_STRING_OBJECT_TYPE = new TypeReference<>() {
    };

    private final ErrorResolver resolver;
    private final ObjectMapper mapper;
    private final ErrorAttributes errorAttributes;

    public CustomBasicErrorController(
            ErrorAttributes errorAttributes,
            ErrorProperties errorProperties,
            ErrorResolver resolver,
            ObjectMapper mapper) {
        super(errorAttributes, errorProperties);
        this.resolver = resolver;
        this.mapper = mapper;
        this.errorAttributes = errorAttributes;
    }

    @Override
    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
        Throwable throwable = errorAttributes.getError(new ServletWebRequest(request));

        ResponseEntity<ErrorMessage> responseEntity = Optional.ofNullable(throwable)
                .map(resolver::resolve)
                .orElseGet(() -> resolver.resolve(getStatus(request)));

        return convert(responseEntity);
    }

    private ResponseEntity<Map<String, Object>> convert(ResponseEntity<ErrorMessage> sourceResponseEntity) {
        return ResponseEntity.status(sourceResponseEntity.getStatusCode()).body(toMap(sourceResponseEntity.getBody()));
    }

    private Map<String, Object> toMap(ErrorMessage errorMessage) {
        return mapper.convertValue(errorMessage, MAP_STRING_OBJECT_TYPE);
    }

}
