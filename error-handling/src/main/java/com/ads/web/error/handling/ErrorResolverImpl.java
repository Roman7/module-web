package com.ads.web.error.handling;

import static java.util.Optional.ofNullable;

import com.ads.web.error.ErrorMessage;
import com.ads.web.error.HttpErrorMessage;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 * Resolves error message by configured mappings. Use {@link ErrorResolverImpl#builder(HttpStatus, int, String, String)}
 * to configure mappings.
 */
@AllArgsConstructor
public final class ErrorResolverImpl implements ErrorResolver {

    private final ErrorMapping defaultErrorMapping;
    private final Map<HttpStatus, ErrorMapping> mappingsByHttpStatus;
    private final Map<Class<? extends Throwable>, ErrorMapping> mappingsByException;

    @Override
    public ResponseEntity<ErrorMessage> resolve(HttpStatus status, Throwable throwable) {
        ErrorMapping errorMapping = mappingsByHttpStatus.getOrDefault(status, defaultErrorMapping);

        ErrorMessage errorMessage = new ErrorMessage(
                errorMapping.getHttpErrorMessage().getErrorMessage().getCode(),
                errorMapping.getHttpErrorMessage().getErrorMessage().getMessage(),
                getAuxMessage(throwable, errorMapping));
        return ResponseEntity.status(status).body(errorMessage);
    }

    @Override
    public ResponseEntity<ErrorMessage> resolve(HttpStatus status) {
        ErrorMapping errorMapping = mappingsByHttpStatus.getOrDefault(status, defaultErrorMapping);

        ErrorMessage errorMessage = new ErrorMessage(
                errorMapping.getHttpErrorMessage().getErrorMessage().getCode(),
                errorMapping.getHttpErrorMessage().getErrorMessage().getMessage(),
                status.getReasonPhrase());
        return ResponseEntity.status(status).body(errorMessage);
    }

    @Override
    public ResponseEntity<ErrorMessage> resolve(Throwable throwable) {
        return findErrorMapping(throwable.getClass()).map(errorMapping -> {
            if (errorMapping.isCommonErrorMapping()) {
                return buildCommonHttpResponse(errorMapping, throwable);
            } else {
                return Optional.ofNullable(errorMapping.getCustomHttpErrorMessageExtractor())
                        .map(customMapping -> customMapping.apply(throwable))
                        .map(this::buildCustomHttpResponse)
                        .orElse(buildCommonHttpResponse(defaultErrorMapping, throwable));
            }
        }).orElse(buildCommonHttpResponse(defaultErrorMapping, throwable));
    }

    @Override
    public boolean isConfiguredForHandling(Class<? extends Throwable> exceptionType) {
        return findErrorMapping(exceptionType).isPresent();
    }

    private ResponseEntity<ErrorMessage> buildCustomHttpResponse(HttpErrorMessage httpErrorMessage) {
        return ResponseEntity.status(httpErrorMessage.getHttpStatus()).body(httpErrorMessage.getErrorMessage());
    }

    private ResponseEntity<ErrorMessage> buildCommonHttpResponse(ErrorMapping errorMapping, Throwable throwable) {
        ErrorMessage errorMessage = new ErrorMessage(
                errorMapping.getHttpErrorMessage().getErrorMessage().getCode(),
                errorMapping.getHttpErrorMessage().getErrorMessage().getMessage(),
                getAuxMessage(throwable, errorMapping));
        return ResponseEntity.status(errorMapping.getHttpErrorMessage().getHttpStatus()).body(errorMessage);
    }

    private static String getAuxMessage(Throwable throwable, ErrorMapping errorMapping) {
        return ofNullable(errorMapping.getAuxMessageExtractor())
                .map(extractor -> extractor.apply(throwable))
                .orElse(ofNullable(errorMapping.getHttpErrorMessage().getErrorMessage().getAuxMessage())
                        .orElse(throwable.getMessage()));
    }

    private Optional<ErrorMapping> findErrorMapping(Class<? extends Throwable> exceptionClass) {
        for (Class<?> exceptionTypeToCheck = exceptionClass; exceptionTypeToCheck != Object.class;
                exceptionTypeToCheck = exceptionTypeToCheck.getSuperclass()) {

            ErrorMapping errorMapping = mappingsByException.get(exceptionTypeToCheck);
            if (errorMapping != null) {
                return Optional.of(errorMapping);
            }

        }
        return Optional.empty();
    }

    /**
     * Builder is initiated with default parameters for default Mapper
     */
    public static MappingBuilder builder(HttpStatus defaultHttpStatus,
            int defaultErrorCode, String defaulterrorMessage, String defaultErrorAuxMessage) {
        return new MappingBuilder(defaultHttpStatus, defaultErrorCode, defaulterrorMessage, defaultErrorAuxMessage);
    }

    /**
     * Builder to build configured {@link ErrorResolverImpl} with mappings by http statuses and exception types
     */
    public static final class MappingBuilder {

        private final ErrorMapping defaultErrorMapping;
        private final Map<HttpStatus, ErrorMapping> mappingsByHttpStatus;
        private final Map<Class<? extends Throwable>, ErrorMapping> mappingsByException;

        private MappingBuilder(HttpStatus httpStatus, int errorCode, String errorMessage, String errorAuxMessage) {
            defaultErrorMapping = new ErrorMapping(httpStatus, errorCode, errorMessage, errorAuxMessage);
            mappingsByHttpStatus = new EnumMap<>(HttpStatus.class);
            mappingsByException = new HashMap<>();
        }


        public MappingBuilder byStatus(HttpStatus httpStatus, int errorCode, String errorMessage) {
            ErrorMapping errorMapping = new ErrorMapping(httpStatus, errorCode, errorMessage);
            mappingsByHttpStatus.put(httpStatus, errorMapping);
            return this;
        }

        public MappingBuilder byStatus(HttpStatus httpStatus, int errorCode, String errorMessage,
                String auxMessage) {
            ErrorMapping errorMapping = new ErrorMapping(httpStatus, errorCode, errorMessage, auxMessage);
            mappingsByHttpStatus.put(httpStatus, errorMapping);
            return this;
        }

        public MappingBuilder byException(Class<? extends Throwable> throwableType,
                HttpStatus httpStatus, int errorCode, String errorMessage) {
            ErrorMapping errorMapping = new ErrorMapping(httpStatus, errorCode, errorMessage);
            mappingsByException.put(throwableType, errorMapping);
            return this;
        }

        public MappingBuilder byException(Class<? extends Throwable> throwableType,
                HttpStatus httpStatus, int errorCode, String errorMessage, String auxMessage) {
            ErrorMapping errorMapping = new ErrorMapping(httpStatus, errorCode, errorMessage, auxMessage);
            mappingsByException.put(throwableType, errorMapping);
            return this;
        }

        @SuppressWarnings("unchecked")
        public <T extends Throwable> MappingBuilder byException(Class<T> throwableType,
                HttpStatus httpStatus,
                int errorCode,
                String errorMessage,
                Function<T, String> auxMessageExtractor) {
            ErrorMapping errorMapping = new ErrorMapping((Function<Throwable, String>) auxMessageExtractor,
                    httpStatus, errorCode, errorMessage);
            mappingsByException.put(throwableType, errorMapping);
            return this;
        }

        public <T extends Throwable> MappingBuilder byException(Class<T> throwableType,
                Function<Throwable, HttpErrorMessage> customErrorMessageMapping) {
            ErrorMapping errorMapping = new ErrorMapping(customErrorMessageMapping);
            mappingsByException.put(throwableType, errorMapping);
            return this;
        }

        public ErrorResolver build() {
            return new ErrorResolverImpl(defaultErrorMapping, mappingsByHttpStatus, mappingsByException);
        }

    }

}
